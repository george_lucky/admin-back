from aiopg.sa import create_engine
import asyncio
#  from settings.production import DB_CONNECTOR_SETTINGS

DB_CONNECTOR_SETTINGS = {
    "host": "localhost",
    "port": 5431,
    "db_name": "postgres",
    "db_user": "postgres",
    "db_password": "postgres",
}


async def do_engine():
    async with create_engine(user=DB_CONNECTOR_SETTINGS.get('db_user', 'postgres'),
                             database=DB_CONNECTOR_SETTINGS.get('db_name', 'wl'),
                             host=DB_CONNECTOR_SETTINGS.get('host', 'localhost'),
                             password=DB_CONNECTOR_SETTINGS.get('db_password', 'postgres'),
                             port=5431) as engine:
        async with engine.acquire() as conn:
            await conn.execute()

asyncio.run(do_engine())
